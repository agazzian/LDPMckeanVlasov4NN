\documentclass{amsart}

\usepackage{comment}
\usepackage{fullpage}
\usepackage{pkg/aageneral}
\usepackage{pkg/aamath}
\usepackage{pkg/mhequ}
\usepackage{pkg/aachem}
\usepackage{mathrsfs}
\usepackage{microtype}


\def\dpr{{\delta'}}
\def\PPG{\PP_t^G}
\def\tt{\tau}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{2}

\begin{document}
\title{Summary Shanghai: precise LDPs for weakly interacting particles}
\maketitle
\section{Notation and problem setting}

We consider a system of $N$ weakly interacting particles with state $X_\tau^{(i)} \in \Rr^d$ for $i \in (1,\dots, N)$. These particles obey the set of \abbr{sde}s
\begin{equ} \label{e:sde0}
  \d X_\tau^{(i)} = \int_{\Rr^d} \nabla_1 V(X_\tau^{(i)},\mu_\tau^{(N)}) \, \d \tau + \frac{1}{\sqrt{N}} \d B_\tau^{(i)} \qquad \text{with} \quad \LL\pc{ (X_0^{(i)})_{i \in (1, \dots, N)} } = \mu_0^{ \otimes N}\,,
\end{equ}
where $V~:~\Rr^d \times \Rr^d \to \Rr$ is the interaction potential, $\mu_\tau^{(N)}(x) := N^{-1} \sum_{i = 1}^N \delta_{X_\tau^{(i)}}(x)$ is the empirical measure of the particles, for all $i \in (1,\dots, N)$, $B_t^{(i)}$ are independent $d$-dimensional Wiener processes and $\LL((X_0^{(i)})_{i \in (1, \dots, N)})$ denotes the law of $X_\tau^{(i)}$ at $\tau = 0$.
We change the time of the original dynamics such that $\tau = N\alpha_N t$  and obtain the \abbr{sde}s\footnote{the timescale $\mathcal O(1)$ in $\tau$ corresponds to the timescale $\mathcal O((N\alpha_N)^{-1})$ in $t$, \ie the characteristic timescale of $t$ is \emph{longer}, if $\alpha_N > 1/N$, than the one of $\tau$.}
\begin{equ} \label{e:sde}
  \d X_t^{(i)} = - N \alpha_N \nabla_1 V(X_t^{(i)},\mu_t^{(N)})\, \d t + \sqrt{\alpha_N}\d B_t^{(i)}\,,
\end{equ}
with the same initial condition as \eref{e:sde0}.

\section{Abstract description}
We aim to establish a \emph{precise} \abbr{ldp} in $C(\MM_1(\Rr^d),[0,T])$ on different timescales, \ie for different values of the parameter $\alpha_N$ in \eref{e:sde}.
To do so, we adapt the results from \cite{dg94},
where the desired \abbr{ldp} is obtained
on the timescale $\alpha_N = 1/N$ (equivalently $t \sim \tau \sim \OO(1)$)
for a \emph{quenched} initial condition $\mu_0^*$,
\ie where the initial conditions of the particles are not random. Such \abbr{ldp} with {quenched} initial condition is called large deviations \emph{system}.
\\
The proof of \cite{dg94} is divided into 3 main steps. The first extends a large deviations system for probability measures $P_{x}^N$ on a topological space $Y$ and parameters $x$ in a topological space $X$ with rate $\gamma_N$ (which is assumed to hold) to one on $\MM_1(Y)$ for the empirical measures of $N$ independent particles on $Y$ with rate $N \gamma_N$. More specifically, they obtain a large deviations principle on the the image of the product space $P^{N}_{x_1} \otimes \dots \otimes P^N_{x_N}$ under the map $(y_1,\dots, y_N) \mapsto \mu^{(N)} = N^{-1} \sum_{i=1}^N \delta_{y_i}$ for fixed parameter sequences $\{x_i\}_{i = 1, \dots, N}$ with $N^{-1} \sum_{i = 1}^N \delta_{x_i} \to \mu_0^*$.\\
Setting $Y = C(\Rr^d,[0,T])$, $X = \Rr^d$ and $\alpha_N = N^{-1}$, a Wentzell-Freidlin \abbr{LDP} in $Y$ for independent particles (that evolve according to \eref{e:avsde} with $\epsilon_N = N^{-1/2}$) with rate $N$ is extended by the above result to one in $\MM_1(Y)$, \ie for their empirical measure in the space of paths for quenched initial measures $\mu_0^{(N)} \to \mu_0^* \in \MM_1(X)$ with rate $N^2$. This \abbr{ldp} is then contracted to one on the flows of marginals, \ie in $C(\MM_1(\Rr^d),[0,T])$ with the same rate and with rate function
$S~:~C(\MM_1(\Rr^d),[0,T]) \to \Rr_+$.\\
 Finally, the desired \abbr{ldp} on the empirical measure process for weakly coupled particles is obtained by Girsanov transformation of the \abbr{ldp} obtained above by contraction. The local part of this result is proven by first separating by H\"older inequality the probability measure for independent particles from the integrated Girsanov exponential (for a cost small at will) and by then using the martingale property of the latter to neglect its contribution. We remark that, for $\alpha_N> 0$ the exponent of the Girsanov transformation scales as
 \begin{equs}
   \pq{A}_T &=  \pq{\int_0^\cdot \sum_{i = 1}^N N \alpha_N (\nabla V(X_t^{(i)},\mu_t^{(N)})-\nabla V(X_t^{(i)},\bar \mu_t)) \sqrt{\alpha_N}^{-1} \d B_t^{(i)}}_T \\&= N  (\alpha_N N^2 ) {\int_0^T\int_{\Rr^d} |\nabla V(x,\mu_t^{(N)})-\nabla V(x,\bar \mu_t))|^2 \mu_t^{(N)}(\d x)\, \dt}\,,
 \end{equs}
 where the first $N$ on the \abbr{rhs} results from the independence of the Brownian motions $B_t^{(i)}$ and $\bar \mu_t\in C(\MM_1(\Rr^d),[0,T])$. This coincides with the result in \cite{dg94} for $\gamma_N = N$ when $\alpha_N = N^{-1}$. A more detailed description of this proof is given below.

We aim to adapt the proof above to the case of \eref{e:sde}. First of all, this requires \emph{precise} Wentzell-Freidlin \abbr{ldp} on timescales $N \alpha_N$. Precise \abbr{ldp}s on the Wiener space have been obtained in \cite{benarous88, azencott85}. Such precise results should therefore be adapted to the present time-changed setting. Then, the proof in \cite{dg94} outlined above is to be made precise, along the lines of the result \cite{benarous90}\footnote{In this sense, it should be checked that both the first, general step of the proof in \cite{dg94} and the Girsanov change of measure can be adapted to a precise setting}. Furthermore, the result in \cite{dg94} should be extended from quenched to annealed in the distribution of initial conditions $X_0^{(i)}$, allowing such initial conditions to be drawn \emph{iid} from some initial measure $\mu_0^*$. In this sense, the quenched result provides an \abbr{ldp} on the probability measure of interest conditioned on a fixed distribution for the initial conditions of the diffusions.
  Assuming that the {quenched} \abbr{ldp} can be made precise, in the annealed case we expect to obtain an asymptotic expansion of the following form: letting $P^{N}$ be the probability distribution on the sample paths of the annealed, $N$-particle empirical measure process, for $G \in C(\MM_1(\Rr^d),[0,T])$
 \begin{equ}
   P^N[G] = \exp \pq{-N \gamma_N \inf_{\mu(\cdot) \in G} S(\mu(\cdot)) - N \inf_{\mu(\cdot) \in G} H(\mu(0)|\tilde \mu_0)}\,,
\end{equ}
where $H(\cdot|\cdot)$ denotes the relative entropy and the corresponding term results from Sanov theorem on empirical measures of independent random variables.
This result suggests, whenever $\alpha_N < \OO(1/N)$, that if the set $G$ includes a trajectory that minimizes the large deviation functional $S(\cdot)$, then the rare event probabilities are dominated by the initial conditions. At timescales of higher order we expect the diffusions to have converged towards an $\alpha_N$-dependent neighborhood of the attractor. This attrractor coincides with the minima of the potential $V$, which is degenerate on manifolds. In this sense, the work of Bolthausen on sums of \emph{iid} random variables can provide directions on how to tackle the proof of \emph{precise} \abbr{ldp}s for potentials of this type. In the case of transversally nondegenerate potentials, the second derivative of such potential around the minimum (parametrized by the longitudinal coordinates) will be the relevant quantity for estimates on the fluctuations around that minimum, contributiong to the desired \abbr{clt}.
\\[10pt]


\section{Sketch of proof}
Recall the definition of a \emph{large deviations system} from \cite{dg94}: we say that the set of probability measures  $\Pi = \{P_x^N, x \in X_N, N \in \NN\}$ on a topological space $Y$ is a \emph{large deviations system} with rate function $I~:~X \times Y \to \RR$ if for each \emph{fixed} $x \in X$ and each sequence $\{x_N\}$ converging to $x$ such that $x_N \in X_N$ for all $N \in \NN$, the probability measures $P_{x_N}^N$ satisfies a \abbr{LDP} with rate function $I(x,\cdot)$\,. This definition introduces a family of \abbr{LDP}s that is \emph{quenched} on the choice of the parameter $x$. \\[5pt]
The proof of the result of \cite{dg94} is structured as follows:
\begin{myenum}
  \item Let $Y, X$ be two topological spaces and assume that the family of probability measures $\{P_x^N, x \in X_N, N \in N\}$ is a large deviations system with rate $\gamma_N$ and rate function
  $$I(x,y) = \begin{cases} J(y) & \text{if } \pi(y) = x\\\infty &\text{else} \end{cases}\,,$$
  for a certain surjective map $\pi~:~Y \to X$ and rate function $J(\cdot)$ on $Y$. Show that the family of probability measures $\{\PP_\mu^{N}, \mu \in \MM_1^{N}(X), N \in \NN\}$, where $\PP_\mu^{N}$ is the image of the product measure $P_{x_1}^N \otimes \dots \otimes P_{x_N}^N$ \abbr{wrt}
  \begin{equ}
    Y^N \ni (y_1, \dots, y_N) \mapsto N^{-1} \sum_{i = 1}^N \delta_{y_i} \in \MM_1(Y)\,.
    \end{equ}
  is a large deviations system on $\MM_1(Y)$ (more precisely $\MM^{N}_1(Y)$) with rate $N \gamma_N$ and rate functional $S~:~\MM_1(Y) \to \Rr_+$ given by
  \begin{equ}
    S(\nu) := \begin{cases}\int_Y J(y) \nu(\d y) & \text{if } \hat \pi(\nu) = \mu\\ \infty& \text{otherwise} \end{cases}\,,
  \end{equ}
  where $\hat \pi$ is the map induced by $\pi$ on $\MM_1(Y)\to\MM_1(X)$.
  %\begin{proof}[Sketch of proof]tba
  %\end{proof}
  \item Apply the above result to obtain a quenched \abbr{LDP} on $\MM(C(\Rr^d,[0,T]))$ for $N$ independent diffusions of the form
  \begin{equ} \label{e:avsde}
    \d \bar X_t^{(i)} = -  \nabla V(\bar X_t^{(i)},\bar \mu_t)\, \d t +\epsilon_N \d B_t^{(i)}\,,
  \end{equ}
  with generator $\LL^{\epsilon_N}(\bar \mu_t)$ for
  \begin{equ}
    \LL^{\epsilon}(\mu) := -  \nabla V(x, \mu) \frac{\partial}{\partial x}  + \epsilon \frac{\partial^2}{\partial x^2}\,.
  \end{equ}
  %and where for every $N$ the probability measure $\bar \mu_t$ obeys the Vlasov \abbr{pde}
  %\begin{equ}
  %\frac{\partial}{\partial t} \bar \mu_t = \LL^{\epsilon_N}(\bar \mu_t)^* \bar \mu_t\,,
  %\end{equ}
  %with initial condition $\bar \mu_0 = \mu_0^*$.
  To do so, first identify $X = \Rr^d$, $Y = C(\Rr^d,[0,T])$. Recall the well-known Wentzell-Freidlin \abbr{LDP} in $C(\Rr^d,[0,T])$ for $\epsilon_N$-noise diffusions with rate $\gamma_N = \epsilon_N^{-2}$ and rate function $J(y)$.
   Then, obtain the desired quenched \abbr{LDP} on $\MM_1(Y)$ by combining the Wentzell-Freidlin \abbr{LDP} on \eref{e:avsde} with the above general result for a deterministic (quenched) sequence of empirical measures at $t = 0$, $\mu_0^N \to \mu_0^*$.
  \item Contract the quenched \abbr{ldp} on $\MM_1(C(\Rr^d,[0,T]))$ for independent particles to an \abbr{LDP} on $C(\MM_1(\Rr^d),[0,T])$ (also quenched on the initial condition $\mu_0^*$), for which the rate function reads:
  \begin{equ}
    I(\mu_t) := \begin{cases} \int_0^T \|\LL^{0}(\mu_t)^*\mu_t - \partial_t \mu_t\|_{\mu_t}^2 \d t &\text{if } \mu_0 = \mu_0^*\\\infty &\text{else} \end{cases}\,.
  \end{equ}
  %\begin{proof}[Sketch of proof]
  %\end{proof}
  \item By Girsanov theorem, transform the \abbr{LDP} for non-interacting particles obtained above into one for weakly interacting particles from \eref{e:sde}. Defining by $\mathbf P_{\mathbf x}^N$ the probability distribution on $C(\Rr^d,[0,T])$ of particles satisfying \eref{e:sde0} with $\epsilon_N = 1/\sqrt{N}$ and initial condition $\mathbf x = (x_1,\dots, x_N)$ and $\bar{\mathbf P}_{\mathbf x}^N := \bar P^{N}_{x_1}
  \otimes \dots \otimes \bar P^N_{x_N}$,
  the change of measure reads
  \begin{equ} \label{e:girsanov}
  \frac{\d {\mathbf P}_{\mathbf x}^{N}}{\d \bar {\mathbf P}_{\mathbf x}^{N}} = \exp \pc{ A_T- \frac12 [A]_T]}
\end{equ}
for $A_T = \int_0^T \sum_{i = 1}^N \pc{\nabla V(\bar X_t^{(i)},\bar\mu_t) - \nabla V(\bar X_t^{(i)},\mu_t^{(N)})} \epsilon_N^{-1} \d B_t^{(i)}$.
Note that $[A]_T$ scales with $N$ because of the independence of the Brownian motions $B_t^{(i)}$.
  \begin{proof}[Sketch of proof] This proof divides into three parts: exponential tightness of the measure $P^{N,\epsilon}$, local lower and upper bound. We concentrate on the proof of the local upper bound, as the lower bound is analogous. By \cite[Assumption (M4)]{dg94} (Continuity of the functional $\mu_t\mapsto \int_0^T \langle \mu_t,|\nabla V(\cdot,\mu_t) - \nabla V(\cdot,\bar \mu_t)|^2 \rangle \d t)$ at $\bar \mu_t$) there exists an open neighborhood $\NN(\bar \mu_t)$ where we can separate the quadratic term in \eref{e:girsanov} into a multiple of the original quadratic variation and a small ``cost'' $\delta$:
    \begin{equ}[]
      [A]_T = -\pc{1-p}\delta N \gamma_N - p [A]_T\,.
    \end{equ}
    Denote by ${\mathscr P^N_\mu}$ the probability distribution of the empirical measure process associated to $\mathbf P_{\mathbf x}^{N}$. By Hölder inequality and recalling that $\gamma_N = \epsilon_N^{-2}$ we obtain for $\mathbf x(\cdot) \in C(\Rr^d,[0,T])$ the local bound
    \begin{equs}
    \mathscr P^N_\mu(\NN(\bar \mu_\cdot))=&\mathbf P_{\mathbf x}^{N}(\mu^{(N)}(\mathbf x(\cdot))\in \NN(\bar \mu_\cdot))\leq e^{-\delta N \gamma_N (1-p)/2} \bar {\mathbb E}^{N}\pc{ \exp\pq{A_T-\frac p 2[A]_T} \indicator_{\mu^{(N)}(\mathbf x(\cdot))\in \NN(\bar \mu_\cdot)}}\\
    & \leq e^{-\delta \gamma_N N (1-p)/2} \bar {\mathbb E}^{N}\pc{ \exp\pq{pA_T-\frac 12[pA]_T}} {\mathcal P}_{\mu_0^*}^{N}\pc{ \NN(\bar \mu_\cdot)}^{1/q} \\
    &\leq e^{-\delta N \gamma_N (1-p)/2} \bar {\mathcal P}_{\mu_0^*}^{N}\pc{ \NN(\bar \mu_\cdot)}^{1/q} \,.\label{e:holder}
  \end{equs}
   where it was used that $\exp\pq{pA_T-1/2[pA]_T}$ is an exponential supermartingale. The proof is concluded by applying the \abbr{ldp} from previous point to $\bar \PP_{\mu_0^*}^N$ and for any $\eta > 0$ taking $\delta >0, q>1$ small enough such that $-\delta (p-1)/2 + S(\bar \mu_t)/q \geq S(\bar \mu_t) - \eta$.
  \end{proof}
\end{myenum}


We are also interested in establishing a \abbr{clt} at short time. Here, use exponential contraction of the Wasserstein gradient flow in a $\lambda$-convex potential. However, in the space of measures the potential under consideration is not \emph{strictly} $\lambda$-convex: on a subspace of $\MM_1(\Rr^d)$ this potential is degenerate. Under pure gradient flow dynamics, \ie in the absence of noise, we expect the fluctuation resulting from an \emph{iid} initial condition to shrink to order $\OO(N^{-1})$ (at least) on timescales longer than $\OO(\log(N))$ \cite{rvde18}. This suggests that the initial fluctuations resulting from Sanov theorem are shrunk exponentially in $t$, as it would happen if the potential were strictly convex. This could be proven by a contraction argument: representing the action at time $t$ of the gradient flow on a measure $\mu$ by $\Phi_t(\mu)$ and taking $t\sim \OO(\log(N))$ we could contract the Sanov-type \abbr{ldp} on the initial distribution to obtain another one (at a different rate), on the distribution at long timescales.

\bibliographystyle{JPE.bst}
\bibliography{bib.bib}
%Throughout, we assume that $X_0^i$ are drawn \emph{iid} from a distribution $\mu_0$.
\begin{comment}
\newpage
\begin{myitem}
  \item Notations minimales
  \item Weak Fluctuations: Ben arous Brunaud
  \item Sum of \emph{iid} Bolthausen? (Paper degenerate on manifold)
  \item $\exp(-N^2 I - N J)$ with $I  = 0$ on $K$. This is transversally nondegenerate. Consider for example the case $$I(x) = C(x_1)x_2^2$$
  \item Tensor product, see it as a finite sum and then let the sum go to infinity
  \item Paper of DG, preuve plus precise
  \item Precise wentzell-freidlin: Ben Arous, Azencott
  \item Write totally abstract part in words.
  \item Proceed sequentially: non interacting diffusions, how do things change with $\alpha_N N^2$ at the initial scale (?)
  \item Annealed when $\alpha_N = N^{-1}$. More specifically until $alpha_N = N^{-1}$ we feel the initial conditions including in the rare events. In this case the measure of the flow is given by the initial conditions.
  \item Then, we are in a $\alpha_N$-dependent neighborhood of the attractor. What happens if the minimum is degenerate?
\end{myitem}
\end{comment}






\end{document}
