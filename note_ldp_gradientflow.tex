\documentclass{amsart}

\usepackage{comment}
\usepackage{fullpage}
\usepackage{pkg/aageneral}
\usepackage{pkg/aamath}
\usepackage{pkg/mhequ}
\usepackage{pkg/aachem}
\usepackage{mathrsfs}
\usepackage{microtype}


\def\dpr{{\delta'}}
\def\PPG{\PP_t^G}
\def\tt{\tau}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{2}

\begin{document}
\title{Note on LDP for gradient flow dynamics}
\maketitle
\section{Notation and problem setting}

We consider a system of $N$ weakly interacting particles with state $X_\tau^{(i)} \in \Theta$ for $i \in (1,\dots, N)$. These particles obey the set of \abbr{ode}s
\begin{equ} \label{e:ode0}
  \frac \d\dt X_t^{(i)} = - \nabla_1 V(X_t^{(i)},\mu_t^{(N)}) \, \qquad \text{with} \quad \LL\pc{ (X_0^{(i)})_{i \in (1, \dots, N)} } = \mu_0^{ \otimes N}\,,
\end{equ}
where $V~:~\Theta \times \Theta \to \Rr$ is the interaction potential, $\mu_\tau^{(N)}(x) := N^{-1} \sum_{i = 1}^N \delta_{X_\tau^{(i)}}(x)$ is the empirical measure of the particles and $\LL((X_0^{(i)})_{i \in (1, \dots, N)})$ denotes the law of $X_t^{(i)}$ at $t = 0$. We further define the corresponding \emph{Vlasov} equation as
\begin{equ}\label{e:pde0}
  \partial_t \mu_t = \nabla \cdot(\mu_t \nabla_1 V(\cdot,\mu_t))\,,
\end{equ}
for an initial condition $\mu_0 \in \MM_+^1(\Theta)$, which is to be understood in the weak sense.
Throughout, we define by $\Phi_t$ the flow on $\MM_+^1(\Theta)$ defined by \eref{e:pde0}. Furthermore we introduce the energy
\begin{equ}\label{e:energy}
  \EE[\mu] := \frac 12 \int_{\Omega}\pc{\int_{\Theta} \phi(x,\theta) \mu(\d \theta) - f^*(x) }^2 \nu(\d x)\,,
\end{equ}
where $\phi(x,\theta) := \theta_0 \sigma(x \cdot \hat \theta)$ and $\nu(\,\cdot\,)$ is the measure of the data. In this case we have
\begin{equ}
  V(\theta, \mu) := \frac {\delta\EE[\mu]}{\delta \mu}(\theta) = -\int_{\Theta} K(\theta, \theta') \mu(\d \theta) + F(\theta)\,,
\end{equ}
for $K(\theta, \theta') = \int_{\Omega}\phi(x,\theta)\phi(x,\theta') \nu(\d x) $ and $F(\theta) :=  \int_{\Omega}\phi(x,\theta)f^*(x) \nu(\d x)$.

Since the initial parameters $X_0^{(i)}$ are sampled \emph{iid} from a measure $\hat \mu_0$ we can apply Sanov theorem to study the large deviations of their empirical measure:
\begin{equ}
  \lim_{N \to \infty} \frac 1 N \p{\mu_0^{(N)} \in A} \asymp - \inf_{\mu_0 \in A}\HH(\mu_0|\hat \mu_0)\,.
  \end{equ}
  The rate of the above LDP suggests the existence of a CLT, formally obtained by the inverse of the Hessian of the rate function.
  \begin{question}
    We would like to show that at large timescales $\mathcal O(N)$ (or even $\mathcal O(\log N)$?) the above CLT vanishes, and the fluctuations of the dynamics shrink to order $\OO(N^{-1})$ (at least) \cite{rvde18}.
  \end{question}

  \section{Relation between CLT and LDP}

  We consider a LDP in the energy $\EE[\mu_t] \in \Rr$. Consider the cumulant generating function $\Lambda_t$  of the distribution of $\EE[\mu_t]$, which by Varadhan's lemma is given by:
\begin{equ}\label{e:cgf}
  \Lambda_t(\lambda) := \lim_{N \to \infty}\frac 1N \log \Ex{\hat \mu_0}{e^{N \lambda \EE[\mu_t]}} = \sup_{\mu_0 \in \NN} \pq{\lambda \EE[\mu_t] - \HH(\mu_0|\hat \mu_0)} = \sup_{\mu_0 \in \NN} \pq{\lambda \EE[\Phi_t (\mu_0)] - \HH(\mu_0|\hat \mu_0)}
\end{equ}
where $\Ex{\hat \mu_0}{\,\cdot\,}$ denotes the expectation wrt empirical measures drawn \emph{iid} from $\hat \mu_0$ and $\NN$ is a bounded set.
  Then the rate function for the LDP for $\EE[\mu_t] \in \Rr$ can be obtained by Cramer-transform of
  \begin{equ}
 I_t(\EE) = \sup_{\lambda \in \Rr}\pq{\EE \lambda - \Lambda_t(\lambda)}\,.
  \end{equ}

Now, we know by \cite{rvde18} that $\EE[\mu_t]$ decreases along trajectories of \eref{e:pde0}, as such trajectories are a gradient flow in this energy. As a consequence of this, the supremum in \eref{e:cgf} can only decrease for positive values of $\lambda$:
\begin{equ}
  {\lambda \EE[\Phi_t (\mu_0)] - \HH(\mu_0|\hat \mu_0)} \leq {\lambda \EE[\mu_0] - \HH(\mu_0|\hat \mu_0)}
\end{equ}
for all $\mu_0 \in \NN$, so that
\begin{equ}
\sup_{\mu_0 \in \NN} \pq{\lambda \EE[\Phi_t (\mu_0)] - \HH(\mu_0|\hat \mu_0)} \leq \sup_{\mu_0 \in \NN} \pq{\lambda \EE[\mu_0] - \HH(\mu_0|\hat \mu_0)}\,,
\end{equ}
while the inverse inequality is true for negative $\lambda$.
  We further recall that the second moment of the distribution of $\EE_t$ is given by
\begin{equ}
  \sigma^2 = \frac {\d^2}{\d \lambda^2}\exp{\pq{\Lambda_t(\lambda)}} \big|_{\lambda=0} = \Lambda_t'(0)^2 + \Lambda_t''(0)\,.
\end{equ}
By the above inequality, the decay of $\EE$ does not imply anything directly about the CLT, as one should control the decay of the derivative $\Lambda'$ wrt the Hessian $\Lambda''$.



\subsection{the geometry of minima of $\mathcal E$}

As a sanity check for the establishment of the long time CLT at a larger scale we aim to understand the geometry of minima of the energy functional \eref{e:energy}
As described in \cite{rvde18} these minimizers must satisfy
\begin{equ}
 - F(y) + \int_{D_0} K(y,y') \d \gamma_0(y') = 0\,,
\end{equ}
We would like to study the geometry of these minimizers.
\begin{example} If we consider an example where $c = 1$, the external potential $V(x)$ is a double-well potential and the interaction potential is not repulsive enough (for example $U(x,y) = |x-y|^2$) and not strong enough to ``convexify'' the external potential, then we know that there exist infinitely many fixed points of the double-well dynamics. These minima are all connected in the space of measures, \ie for all minimizer $\mu_1^*$ there exists another, different minimizer $\mu_2^*$. For this reason I will only have exponential convergence to a certain minimizer onto the set $M_1 = \{\mu~:~ \lim_{t \to \infty} \Phi_t^\#\mu = \mu_1\}$. On this manifold I will therefore see that the (local) CLT vanishes on long times ($\mathcal O(N)$ times for example), and therefore that the rate of the LDP changes, but I will not observe this phenomenon if \eg I am initializing the measure at equilibrium and I am considering a fluctuation which is initialized at the neighboring, arbitrarily close fixed point.
\end{example}
More specifically, we have
\begin{lemma}[?]
  For every global minimizer $\rho^*$ of \eref{e:energy} and for all $\epsilon > 0$ there exists a fixed point $\rho_\epsilon^*$ of \eref{e:pde0} with $\mathcal W_2(\rho^*,\rho_\epsilon^*)$
\end{lemma}

\begin{question}
  I think that if the above lemma is correct (I was thinking to construct a perturbation of the measure $\rho^*$ that lacks support on a very small set) in light of the above example it should not be possible to obtain uniform exponential contraction in the general setting, except if we add noise to the dynamics (which as we discussed ``fattens'' the support of $\rho$). Did you already think about this kind of problem?
\end{question}

The inverse of the is also true:
\begin{lemma}
  For every fixed point $\tilde \rho^*$ of \eref{e:pde0} without full support, there exists $\rho_0$ such that $\Phi_t^\#\rho_0$ converges to a global minimizer of \eref{e:energy} and such that $\mathcal W_2(\rho_0,\tilde \rho^*)$
\end{lemma}



\section{The small-noise LDP}

As described in the previous section, it is essential for the establishment of the \abbr{ldp} to ensure that the quadratic variation \eref{e:qv} is of order $N \alpha_N$. This is clearly the first order of business, as without this estimate, neglecting the Girsanov term as done in \cite{dg87} through an application of Holder inequality.

\begin{question} The application of Holder inequality as explained at the end of the note from Shanghai ensures that the contribution of the Girsanov change of measure is negligible at the scale we are interested in. In our case, however, we would like to go beyond that scale, \ie we are interested in events for which the ``higher rate'' large deviations functional is vanishing (or more precisely of order $\\mathcal O(1/N)$). Then, the fact that one can neglect the Girsanov correction term to the higher order does not mean that this term does not affect the lower order rate function, or that id does not appear at an ``intermediate'' rate. I should ask this question to Gerard.
\end{question}

The paper \cite{eve10} gives a nice way of reducing the dynamics of a stochastic process when it is confined on a submanifold, and treats some example of the same setting in the infinite dimensional case. The same rationale (the presence of an entropic term in the constrained dynamics) is present in the paper \cite{benarous90} (the Hilbert-Schmidt operator appearing in the CLT keeps track of the width of the potential). This approach, however, assumes that the dynamics starts ``inside'' the strong potential well and does not give estimates on the fluctuations of the dynamics in the direction of strong constraint.

\begin{question}
  Would the establishment of the LDP on the transversal, reduced dynamics (assuming we can parametrize the submanifold of fixed points of the flow) be enough to describe the full dynamics and its fluctuations?
\end{question}

\section{Leftovers: Precise LDP at long times}

We want to prove the above result by combining a precise statement for Sanov theorem together with a contraction argument: representing the action at time $t$ of the gradient flow on a measure $\mu$ by $\Phi_t$ (as defined above) and taking $t \sim \OO(\log(N))$ we aim to  contract the Sanov-type \abbr{ldp} on the initial distribution to obtain another one (at a different rate), on the distribution at long timescales.

More specifically, to show that the LDP estimates giving the original CLT at order $\mathcal O(N^{-1/2})$ vanish at large times we write $\p{\mu_t^{(N)} \in A_t} = \p{\mu_0^{(N)} \in A_0}$ where $A_0 := \Phi_{-t}^\# A_t = \{\mu \in \MM_+^1(\Rr^d)~:~\Phi_{t}^\# \mu \in A_t\}$, so that
\begin{equ}\label{e:rhs}
  \frac 1 N \log \p{\mu_t^{(N)} \in A_t} = \frac 1 N \log \p{\Phi_{-t}^\# \mu_t^{(N)} \in \Phi_{-t}^\# A_t} \asymp - \inf_{\nu_0 \in \Phi_{-t}^\# A_t}\HH(\nu_0|\mu_0) = - \inf_{\nu_t \in  A_t}\HH(\Phi_{-t}^\# \nu_t|\Phi_{-t}^\# \Phi_{t}^\# \mu_0)\,.
\end{equ}
To show that the rate function vanishes at large times we have to show that the \abbr{rhs} of \eref{e:rhs} vanishes in the limit $t \sim \OO(N)$ for $N \to \infty$. To do so we attempt to bound from above the decay of objects of the form $\HH(\Phi_{t}^\# \nu_0|\Phi_{t}^\#  \mu_0)$. If we succeed in this sense, we can then apply Gronwall-type bounds to obtain $\HH(\Phi_{t}^\# \nu_0|\Phi_{t}^\#  \mu_0) \leq e^{-\gamma t} \HH( \nu_0| \mu_0) $ (or a similar decay for polynomial-type bounds) Inverting this result we would have
\begin{equ}
  \HH(\Phi_{-t}^\# \nu_t|\Phi_{-t}^\# \Phi_{t}^\# \mu_0) \geq e^{\gamma t} \HH(\nu_t| \Phi_{t}^\# \mu_0)\,,
\end{equ}
which diverges for $t \sim \mathcal O(N)$ when $N \to \infty$ (for a fixed value of $\HH(\nu_t| \Phi_{t}^\# \mu_0)$).

Grant observed that in the recent paper \cite{chizat19}, in the proof of Lemma 3.10 we see the identity
\begin{equ}
  \frac \d \dt \HH(\nu_t^\epsilon, \nu_t) = \int_{\mathcal X} V(x,\nu_t) \cdot (\nu_t(\d x)-\nu_t^\epsilon(\d x))\,,
\end{equ}
for a perturbation $\nu_t^\epsilon$ of $\nu_t$. This may be a good point to start obtaining the bound on the decay of $\HH$, although this would work best if $\nu_t$ converges to a global minimizer of the energy (or else the bound is strictly positive).

Another option would be to try to bound the decay of $\HH$ by the \emph{gradient} of $V$. The reason for this is that for long time intervals, bounding such quantity is also necessary for performing the change of measure from the independent to weakly dependent setting to obtain the small noise LDP: to perform the change of measure at small cost as done in Equation (1.5) in the report from Shanghai we need to show that the integral appearing in the expression for the quadratic variation
\begin{equ}\label{e:qv}
  \pq{A}_T = N  (\alpha_N N^2 ) {\int_0^T\int_{\Rr^d} |\nabla V(x,\mu_t^{(N)})-\nabla V(x,\bar \mu_t)|^2 \mu_t^{(N)}(\d x)\, \dt}
\end{equ}
gives a contribution of order $\mathcal O(\alpha_N^{-1})$ (note that in the last expression, as in the note from Shanghai, we have changed the time of the \abbr{sde}).

\bibliographystyle{JPE.bst}
\bibliography{bib.bib}
%Throughout, we assume that $X_0^i$ are drawn \emph{iid} from a distribution $\mu_0$.
\begin{comment}
\newpage
\begin{myitem}
  \item Notations minimales
  \item Weak Fluctuations: Ben arous Brunaud
  \item Sum of \emph{iid} Bolthausen? (Paper degenerate on manifold)
  \item $\exp(-N^2 I - N J)$ with $I  = 0$ on $K$. This is transversally nondegenerate. Consider for example the case $$I(x) = C(x_1)x_2^2$$
  \item Tensor product, see it as a finite sum and then let the sum go to infinity
  \item Paper of DG, preuve plus precise
  \item Precise wentzell-freidlin: Ben Arous, Azencott
  \item Write totally abstract part in words.
  \item Proceed sequentially: non interacting diffusions, how do things change with $\alpha_N N^2$ at the initial scale (?)
  \item Annealed when $\alpha_N = N^{-1}$. More specifically until $alpha_N = N^{-1}$ we feel the initial conditions including in the rare events. In this case the measure of the flow is given by the initial conditions.
  \item Then, we are in a $\alpha_N$-dependent neighborhood of the attractor. What happens if the minimum is degenerate?
\end{myitem}
\end{comment}






\end{document}
