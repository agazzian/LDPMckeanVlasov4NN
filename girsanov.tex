\documentclass{article}

\usepackage{comment}
\usepackage{fullpage}
\usepackage{pkg/aageneral}
\usepackage{pkg/aamath}
\usepackage{pkg/mhequ}
\usepackage{pkg/aachem}
\usepackage{mathrsfs}


\def\dpr{{\delta'}}
\def\PPG{\PP_t^G}
\def\tt{\tau}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{2}

\begin{document}
\section{Notes on the application of Girsanov theorem}

We consider a system of $N$ weakly interacting particles with state $X_t^i \in \Rr^d$ for $i \in (1,\dots, N)$ and whose empirical measure at time $t$ is denoted by $\mu_t^{(N)}$. The dynamics of the system is described by the \abbr{sde}s
\begin{equ} \label{e:sde0}
  \d X_t^i = - \alpha_N \nabla_1 V(X_t^i,\mu_t^{(N)})\, \dt + \epsilon_N \d B_t^i \qquad \text{with} \quad \LL\pc{ (X_0^i)_{i \in (1, \dots, N)} } = \mu_0^{ \otimes N}\,,
\end{equ}
where $V~:~\Rr^d \times \Rr^d \to \Rr$ is the interaction potential and for all $i \in (1,\dots, N)$, $B_t^i$ is a $d$-dimensional Wiener process and $\LL((X_0^i)_{i \in (1, \dots, N)})$ denotes the law of $X_i$ at $t = 0$. {\color{red}  In the following, we set $\alpha_N = 1$.}
%Throughout, we assume that $X_0^i$ are drawn \emph{iid} from a distribution $\mu_0$.

We are interested in the dynamics of \eref{e:sde0} on the long time scales. For this reason we transform the time according to $t \to \tau = \tau_N^{-1} t$ (we bring $t = \tau_N$ to $\tau = 1$) {\color{red} for $\tau_N =  {\epsilon_N}^{-2} $} and obtain, for $i \in (1, \dots, N)$,
\begin{equ}\label{e:sde0N}
  \d X_\tt^i = - \tau_N \nabla_1 V(X_\tt^i,\mu_t^{(N)})\, \d \tt + \d B_\tt^i\,,
\end{equ}
with the same initial conditions as above.

We want to apply Girsanov theorem to deduce a \abbr{ldp} for the system above from a system of independent particles, to which we can apply Sanov theorem. We start by considering the simplest comparable system, \ie the one of independent particles with evolution
\begin{equ}\label{e:Y}
  \d Y_\tt^i = \d B_\tt^i\,,
\end{equ}
and initial condition as above. We denote the law of the process solving \eref{e:Y} on $(0,T)$ by $\bar \Pp_T(\mu_0)$. In this case, Sanov theorem directly gives a \abbr{ldp} for the empirical measure in the space of paths (\ie on $M_1^+(W_T^d)$) with rate $N$ and rate function $R \mapsto I(R,\bar \Pp_T(\mu_0))$ for $R \in M_1^+(W_T^d)$,
\begin{equ}
  I(\mu,\nu)  := \begin{cases} \int \log(\d \mu/ \d \nu) \d \mu & \text{if }\mu \ll \nu \text{ and } \log(\d \mu/ \d \nu) \in L^1(\d \mu)\\
  \infty & \text{else}
  \end{cases}\,.
\end{equ}

The na{i}ve application of Girsanov theorem adds the following term to the above exponential estimates:
\begin{equ}
  \frac{\d \Pp^{(N)}(\mu_0^{\otimes N})}{\d \bar \Pp(\mu_0)^{\otimes N}}(\tilde w) \sim \exp \pq{L_T^{(N)}(\tilde w)}\,,
\end{equ}
where $\sim$ denotes equality up to a finite normalization factor $\mathcal Z_N$ and for $N$ independent $(W_T^d, \bar \Pp(\mu_0))$-Brownian motions $(\beta_t^i)_{t \in [0,T]}$ for $i \in (1, \dots, N)$ we define
\begin{equs}\label{e:expmeaschange}
  L_T^{(N)}(\tilde w) := - \frac{1}{2} \int_0^T \| \tau_N \nabla_1 V(X_\tt^i,\mu_\tt^{(N)})\|_2^2 \d \tt + \int_0^T  \dtp{\tau_N \nabla_1 V(X_\tt^i,\mu_\tt^{(N)}) , \d \beta_\tt(\tilde w) }\,,
\end{equs}
where $\dtp{\cdot, \cdot}$ denotes the scalar product in $\Rr^d$, $\|\cdot\|_2$ the norm induced by it and  $(\beta_\tt^i)_{\tt \in [0,T]}$ for $i \in (1, \dots, N)$ are $N$ independent $(W_T^d, \bar \Pp(\mu_0))$-Brownian motions.

{\color{red}
By expansion of norm in all its components and extracting $\tau_N$ we obtain
\begin{equ}\label{e:correctionterm}
  \int_0^T \| \tau_N \nabla_1 V(X_\tt^i,\mu_\tt^{(N)})\|_2^2 \d \tt = \tau_N^2 N  \int_0^T | \nabla_1 V(X_\tt^1,\mu_\tt^{(N)})|^2 \d \tt\,.
\end{equ}
In particular, choosing $\tau_N = N$ we obtain a term of order $N^3$ from the naive Girsanov expansion. \\
Had we left $\alpha_N$ in the expansion above, we would have recovered the following formula for the coefficient of the quadratic variation in \eref{e:expmeaschange}
\begin{equ}
 N\alpha_N^2 \tau_N^2\,.
\end{equ}
We also note that without performing the time change, the exponenti in the \eref{e:measchange} becomes
\begin{equs}
  L_T^{(N)}(\tilde w) := - \frac{1}{2} \int_0^T \| \tau_N \nabla_1 V(X_\tt^i,\mu_\tt^{(N)})\|_2^2 \d \tt + \int_0^T  \dtp{\tau_N \nabla_1 V(X_\tt^i,\mu_\tt^{(N)}) , \d \beta_\tt(\tilde w) }\,,
\end{equs}
and
\begin{equ}
  \epsilon_n^{-2}  \int_0^T \| \alpha_N \nabla_1 V(X_t^i,\mu_t^{(N)})\|_2^2 \d t = N \alpha_N^2 \epsilon_n^{-2}  \int_0^T | \nabla_1 V(X_t^1,\mu_t^{(N)})|^2 \d t\,.
\end{equ}}

A less naive approach would be to compare the system \eref{e:sde0} to one whose particles evolve interacting with an ``external'' measure. In this sense we study the evolution of $N$ copies of the process $\bar X_t$ obeying
\begin{equ} \label{e:sde1}
  \d Y_\tt = - \tau_N \int_{\Rr^d} \nabla_1 V(\bar X_\tt,y) \d \bar \mu_\tt^{(N)}(y)\,\, \d \tt + \d \bar B_\tt \qquad \text{with} \quad \LL\pc{ \bar X_0 } = \mu_0\,,
\end{equ}
where for any $t \geq 0$ the measure $\bar \mu_t^{(N)} := \LL(\bar X_\tau)$ is an element of $M_+(\Rr^d)$, the set of measures on $\Rr^d$. In this case, the Girsanov term would have the same coefficient as in the previous analysis, but Sanov is Not directly applicable,as the distribution of $Y_\tt$ depends on $N$.





  \bibliographystyle{JPE.bst}

\end{document}
