\documentclass{article}

\usepackage{comment}
\usepackage{fullpage}
\usepackage{pkg/aageneral}
\usepackage{pkg/aamath}
\usepackage{pkg/mhequ}
\usepackage{pkg/aachem}
\usepackage{mathrsfs}


\def\dpr{{\delta'}}
\def\PPG{\PP_t^G}
\def\tt{\tau}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{2}

\begin{document}
\section{LDP for weakly interacting particles at low temperature}

\subsection{Notation}

We consider a system of $N$ weakly interacting particles with state $X_t^i \in \Rr^d$ for $i \in (1,\dots, N)$. The dynamics of the system is described by the \abbr{sde}s
\begin{equ} \label{e:sde0}
  \d X_t^i = \frac{1}{N} \sum_{j = 1}^N \nabla_1 V(X_t^i,X_t^j)\, \dt + \frac{1}{\sqrt N}\d B_t^i \qquad \text{with} \quad \LL\pc{ (X_0^i)_{i \in (1, \dots, N)} } = \mu_0^{ \otimes N}\,,
\end{equ}
where $V~:~\Rr^d \times \Rr^d \to \Rr$ is the interaction potential and for all $i \in (1,\dots, N)$, $B_t^i$ is a $d$-dimensional Wiener process and $\LL((X_0^i)_{i \in (1, \dots, N)})$ denotes the law of $X_i$ at $t = 0$.
%Throughout, we assume that $X_0^i$ are drawn \emph{iid} from a distribution $\mu_0$.
Note that the scaling of the noise term in \eref{e:sde0} indicates that the system is at very low temperature.

For large $N$ we compare the system \eref{e:sde0} to its mean field counterpart. In this sense we study the evolution of $N$ copies of the process $\bar X_t$ obeying
\begin{equ} \label{e:sde1}
  \d \bar X_t =  \int_{\Rr^d} \nabla_1 V(\bar X_t,y) \d \bar \mu_t^{(N)}(y)\,\, \dt + \frac{1}{\sqrt N}\d \bar B_t \qquad \text{with} \quad \LL\pc{ \bar X_0 } = \mu_0\,,
\end{equ}
where for any $t \geq 0$ the measure $\bar \mu_t^{(N)} := \LL(\bar X_t)$ is an element of $M_+(\Rr^d)$, the set of measures on $\Rr^d$.

We are interested in the dynamics of \eref{e:sde0} and \eref{e:sde1} on the long time scales. For this reason we transform the time according to $t \to \tau = \lambda t$ for $\lambda = N$ and obtain, for $i \in (1, \dots, N)$,
\begin{equ}\label{e:sde0N}
  \d X_\tt^i =  \frac \lambda N\sum_{j = 1}^N \nabla_1 V(X_\tt^i,X_\tt^j)\, \d \tt + \d B_\tt^i\,,
\end{equ}
and
\begin{equ}\label{e:sde1N}
  \d \bar X_\tt =  \lambda \int_{\Rr^d} \nabla_1 V(\bar X_\tt,y) \d \bar \mu_\tt^N(y)\,\, \d \tt + \d \bar B_\tt\,,
\end{equ}
with the same initial conditions as above.
We aim to establish a \abbr{ldp} at large $N$ for the empirical measure in path space of the system \eref{e:sde0N}. By abuse of notation, we are going to indicate the changed time $\tt$ as $t$. Throughout, denote by $\Pp^{(N)}(\mu_0^{\otimes N}) \in M_+(W_T^{Nd})$ and $\bar \Pp(\mu_0) \in M_+(W_T^{d})$ the laws of the process respectively solving \eref{e:sde0N} and \eref{e:sde1N} in the interval $(0,T)$.

\subsection{LDP for the empirical measure in $W_T^d$}

We appy Girsanov's theorem to compare the two measures:
\begin{equ}\label{e:girsanov}
  \frac{\d \Pp^{(N)}(\mu_0^{\otimes N})}{\d \bar \Pp(\mu_0)^{\otimes N}}(\tilde w) \sim \exp \pq{L_T^{(N)}(\tilde w)}\,,
\end{equ}
where $\sim$ denotes equality up to a finite normalization factor $\mathcal Z_N$ and for $N$ independent $(W_T^d, \bar \Pp(\mu_0))$-Brownian motions $(\beta_t^i)_{t \in [0,T]}$ for $i \in (1, \dots, N)$ we define
\begin{equs}\label{e:girsanovexp}
  L_T^{(N)}(\tilde w) := - \frac{1}{2} \int_0^T \| h_t(\tilde w)\|_2^2 \d t + \int_0^T  \dtp{h_t(\tilde w) , \d \beta_t(\tilde w) }\,,
  \end{equs}
  where $\dtp{\cdot, \cdot}$ denotes the scalar product in $\Rr^d$, $\|\cdot\|_2$ the norm induced by it and introducing the symmetric-mean zero function
  \begin{equ}
       V_t(x,y) :=  V(x,y) - \int_{\Rr^d}  V(x, z) \d \bar \mu_t^{(N)}(z) - \int_{\Rr^d}  V(y, z) \d \bar \mu_t^{(N)}(z) + \dtp{  V(y, z)\,,(\bar \mu_t^{(N)})^{\otimes 2}}\,,
  \end{equ}
   we define for $\tilde w \in (W_T^d)^N$
\begin{equ}
  h_t^i(\tilde w):=  \frac \lambda N \sum_{j = 1}^N \nabla_1 V_t(X_t^i(\tilde w),X_t^j(\tilde w)) = \lambda \pc{ \frac 1N \sum_{j = 1}^N \nabla_1 V(X_t^i(\tilde w_i),X_t^j(\tilde w_j)) -  \int_{\Rr^d} \nabla_1 V(X_t^i(\tilde w_i), y) \d \bar \mu_t^{(N)}(y)}\,.
\end{equ}
In order to construct the rate function of the desired \abbr{ldp} in $M_+(W_T^d)$ from this transformation, we proceed to compute the integral $\dtp{\exp(L_T^{(N)}), R^{\otimes N}}$ for $R \in M_+(W_T^d)$. With this in mind, we first manipulate the two terms in \eref{e:girsanovexp} separately. For the first one we have
  \begin{equ}\label{e:timeintegralterm}
    \|h(\tilde w)\|_2^2= \sum_{i = 1}^N (h_t(\tilde w))_i^2   =  \lambda^2 \frac 1 {N^2} \sum_{i,j,k = 1}^N \nabla_1 V_t(X_t^i(\tilde w_i),X_t^j(\tilde w_j))\, \nabla_1 V_t(X_t^i(\tilde w_i),X_t^k(\tilde w_k)) \,.
  \end{equ}
  and by symmetry of $V_t(\cdot, \cdot)$ in its arguments we obtain
  \begin{equ}\label{e:ahat}
     \langle{ \int_0^T \|h(\tilde w)\|_2^2\, \dt, R^{\otimes N} \rangle} = N \lambda^2 \dtp{\hat a_T(w), R^{\otimes 3}}\,,
  \end{equ}
  where we have defined for $w \in W_T^{3d}$ and $\sigma(1,2,3)$ the set of permutations of $(1,2,3)$\,,
  \begin{equ}
    \hat a_T(w) := \frac{1}{3!} \sum_{(i,j,k) \in \sigma(1,2,3)} \int_0^T   \nabla_1 V_t(X_t^i(w_i),X_t^j( w_j))\, \nabla_1 V_t(X_t^i( w_i),X_t^k( w_k)) \dt\,.
  \end{equ}
  Correspondingly, we expand the $\Rr^N$ scalar product in the second term of \eref{e:girsanovexp} as
  \begin{equ}\label{e:martingaleterm}
     \dtp{h_t(\tilde w) , \d \beta_t(\tilde w) } = \sum_{i = 1}^N h_t^i(\tilde w) \d \beta_t^i(\tilde w) =  \frac \lambda N  \sum_{i,j = 1}^N  \nabla_1 V_t(X_t^i(\tilde w_i),X_t^j(\tilde w_j)) \d \beta_t^i(\tilde w)\,,
\end{equ}
and similarly to \eref{e:ahat} we obtain
\begin{equ}
  \dtp{\int_0^T \dtp{h_t(\tilde w) , \d \beta_t(\tilde w) },R^{\otimes N}(\tilde w)} = N \lambda \dtp{\hat b_T(w), R^{\otimes 3} }\,,
\end{equ}
where we have defined for $w \in W_T^{3d}$
\begin{equ}\label{e:bhat}
  \hat b_T(w) := \frac{1}{3!}  \sum_{(i,j,k) \in \sigma(1,2,3)} \int_0^T \nabla_1 V_t(X_t^i( w_i),X_t^j( w_j)) \d \beta_t^i( w) \,.
\end{equ}
Note that $\hat a_T, \hat b_T$ mapping $W_T^{3d}$ to $\Rr$ are invariant under permutation of their arguments.

To obtain the desired \abbr{LDP} for the empirical measure in $W_T^d$ of \eref{e:sde0} we map this space onto a Banach space for which such \abbr{ldp} is derived in  \cite{bolthausen86}. To do so we require the following
\begin{assumption}\label{a:1}
  {The potential $V(x,y)$ can be written as the sum of an external and an interacting potential, respectively denoted by $F(x)$ and $K(x,y)$, satisfying the following assumptions:
\begin{myitem}
  \item $F~:~\Rr^d \to \Rr$ is a member of $C^2$ with bounded derivative and $\exp (-F) \in L^2(\Rr^d)\,,$
  \item $K~:~C^2_b(\Rr^{d}\times \Rr^{d}, \Rr)$ is a member of the projective tensor product space $\tilde \bigotimes_\pi^2 C_b(\Rr^d,\Rr)$ such that there exists a compact measure space $(C,\tilde \nu)$ and a function $g~:~\Rr^d\times C \to \Rr$ differentiable in its first argument such that $K = \int_{C} g(\cdot, \tau)^{\otimes 2} \d \tilde \nu( \tau)$\,.
\end{myitem}
}
\end{assumption}
\rmk{In the case of interest, the measure space $(C, \tilde \nu)$ corresponds to the space of datapoints, which is not compact in general. However, in some specific applications and for any finite set of points the space is indeed compact.}

In light of the above and by application of Sanov's theorem (through contraction principle combined with the results from \cite{bolthausen86}) to the empirical measure on $W_T^d$ for $N$ independent copies of the process \eref{e:sde1N} we write the candidate rate function on $M_+(W_T^d)$ as
\begin{equs}\label{e:rf}
  \FF_T(R) := I(R,\bar \Pp_T(\mu_0))- \Gamma_T^\lambda(R) - m_T^\lambda\,,
\end{equs}
where
\begin{equ}
  I(\mu,\nu)  := \begin{cases} \int \log(\d \mu/ \d \nu) \d \mu & \text{if }\mu \ll \nu \text{ and } \log(\d \mu/ \d \nu) \in L^1(\d \mu)\\
  \infty & \text{else}
  \end{cases}\,,
\end{equ}
is the relative entropy function and
  \begin{equs}
\Gamma_T^\lambda(R) &:=   \lambda\dtp{\hat b_T,R^{\otimes 3}} -\lambda^2\frac{1}{2}\dtp{\hat a_T, R^{\otimes 3}  } \,,\\
m_T^\lambda &:= \inf_{R \in M_+(W_T^d)} \pg{I(R,\bar \Pp_T(\mu_0) ) - \Gamma_T^\lambda(R)}\,,
\end{equs}
are the change of measure and the normalizing factor, respectively.

\subsection{The minimizer of $\FF_T$}

We now proceed to find the minimizer $Q \in M_+(W_T^d)$ of \eref{e:rf}.
%\aa{adapt this part better}
Define $h := \d Q/ \d \bar \Pp_T$, a bounded, zero-average function $\phi \in L^2(W_T^d,\bar \Pp_T)$ and $ Q_u := (1+u \phi) Q$ for $u \in \Rr$ small enough. Then we have
\begin{equ}
  \FF_T(Q_u) - \FF_T(Q) = u \Ex{Q}{\pc{\log h + 3 \dtp{\lambda^2\hat a_T/2 - \lambda \hat b_T, Q^{\otimes 2}}}\phi}\,,
\end{equ}
where the factor $3$ in front of the inner product results form symmetry of $\Gamma_T$ and the integration against $Q^{\otimes 2}$.
Consequently, the minimum must satisfy
\begin{equ}
  \log h  = C_{Q} - 3 \dtp{\lambda^2 \hat a_T/2 - \lambda \hat b_T, Q^{\otimes 2}}\,,
\end{equ}
for a constant $C_{Q} \in \Rr$ or equivalently
\begin{equ}\label{e:hmin}
  h = \mathcal Z_{Q} \exp \dtp{3( \lambda\hat b_T -\lambda^2\hat a_T/2) h^{\otimes 2}, \bar \Pp_T^{\otimes 2} }\,.
\end{equ}

%Finally, we show that the minimizer on long timescales satisfies the McKean-Vlasov \abbr{pde}. Assuming that the Novikov condition is satisfied \aa{check!}
The derivative $\d Q/\d \bar {\mathbb P} _T |_{t}$, which we henceforth denote by $Z_t$, is a $\bar{\mathbb P}_T$-martingale. By Ito's lemma for $\log Z_t$ we have
\begin{equ}\label{e:logZ}
  \log Z_t - \log Z_0 = \int_0^t \frac{\d Z_s}{Z_s} - \frac{1}{2}\int_0^t \frac{\d \dtp{Z}_s}{Z_s^2}\,.
\end{equ}
By Doob's decomposition theorem  we have for the martingale part of \eref{e:logZ}
\begin{equ}
  \int_0^t \frac{\d Z_s}{Z_s} =  3 \lambda \int_0^t \dtp{\hat b_T Z_s^{\otimes 2}, \bar \Pp_T^{\otimes 2} } \d \beta_s\,,
\end{equ}
and consequently by the invariance of the integrand in $\hat b_T$ under exchange of the indices $j \leftrightarrow k$ we have that $\bar \Pp_T$-almost surely
\begin{equ}Z_t = Z_0 + 3 \lambda \int_0^t Z_s \dtp{\hat b_T Z_s^{\otimes 2}, \bar \Pp_T^{\otimes 2} } \d \beta_s = Z_0 +  \lambda \int \int \nabla_1 V_{s}(x_{s}(\,\cdot\,),x_s(w)) Q(\d w) \d \beta_{s} \,.\end{equ}
Using \eref{e:bhat}, we apply again Ito's lemma to the snapshot $Z_t f \circ x_t$ at time $t$ for any compactly supported test function $f$:
\begin{equ}
  Z_tf \circ x_t - Z_\epsilon f \circ x_\epsilon = \int_\epsilon^t Z_{s} \pq{ \nabla f(x_{s}) + \lambda f(x_{s}) \int \nabla_1 V_{s}(x_{s},y) q_{s}(\d y)} \d \beta_{s} + \int \bar  \LL(q_{s})(f)(x_{s}) Z_{s} \d s\,,
\end{equ}
where $q_{t}$ is the marginal of $Q$ on $\Rr^d$ at time $t$ and $\bar  \LL(q_t)$ is the generator of mean field process \eref{e:sde1N}. The desired result is then obtained by taking expectations \abbr{wrt} $\bar{\mathbb P}_T$: the martingale part disappears and we are left with
\begin{equ}\label{e:lbar}
  \dtp{f, q_t} - \dtp{f,  q_0} = \int_0^t \dtp{\bar  \LL( q_s)(f),  q_s} \d s\,.
\end{equ}

\subsection{Short-time results}

We note that a \abbr{ldp} result for the short-time behavior of the $N$-particle system has been obtained \cite{dg94}. We report the result below. Let $U~:\RR^d \to \RR$ be a nonnegative twice continuously differentiable function
such that $U(x) \to \infty$ as $|x| \to \infty$. Given $R > 0$, let $\mathcal M_R$ denote the subspace of
$\mathcal M_1$ consisting of all $\mu$ for which $\langle \mu, U\rangle< R$ , and let $\mathcal C_R$ denote the space
$C([0, T]; J/R)$ furnished with the uniform topology. We introduce a space $\mathcal M_\infty$ of
admissible probability measures and a corresponding space $\mathcal C_\infty$ of measure-valued
paths by setting
\begin{equ}
  \mathcal M_\infty := \bigcup_R \mathcal M_R \qquad \text{and} \qquad \mathcal C_\infty := \bigcup_R \mathcal C_R\,.
\end{equ}
We equip both spaces with the strongest topology which induces on $\mathcal M_R $ and $\mathcal C_R$,
respectively, the given topology for each $R > 0$. We assume that
\begin{assumption}\label{a:1}
  \begin{myenum}
  \item The maps $V: \RR^d \to \RR^d \times \RR^d$ and $U: \RR \times \mathcal M_\infty\to \RR^d$ are continuous.
For each $x \in \RR^d$, the matrix $V(x)$ is symmetric and strictly positive definite.
\item There exists a constant $\lambda \geq 0$ s.t.
$$\langle \mu , \LL_N(\mu) U + \frac 12|\nabla U|^2\rangle < \lambda \langle \mu,U \rangle\,, $$
for all probability measures $\mu$ with compact topological support, and for all $N > 0$.
\item For each $\bar \mu(\cdot ) \in \mathcal C_\infty$ there exists a constant $\bar \lambda \geq 0$ s.t.
\begin{equ}
  \LL_N(\mu_t) U + \frac 12|\nabla U|^2 \leq \bar \lambda U\,,
\end{equ}
for all $t \in [0,T]$ and all $\epsilon \in (0,1)$\,. Furthermore the function
\begin{equ}
  \bar \mu(\cdot) \mapsto \int_0^T \langle  \mu_t, |b(\cdot, \mu_t) - b(\cdot, \bar \mu_t)|\rangle \dt \in [0,\infty]\,,
\end{equ}
is sequentially continuous at $\mu(\cdot) = \bar \mu(\cdot)$\,.
\end{myenum}
\end{assumption}
We note that the asssumptions above are the same as the ones in \cite{dg87}. Here, $U$ is a Lyapunov function.
\begin{theorem}
  Let the \aref{a:1} be satisfied. Then $\Pi^N_{\mu_0}$ (the law of the empirical process of the $N$-particle process with initial distribution $\mu_0$) obeys a large deviation principle with rate $N^2$ as $N \to \infty$ and rate function $S_0$  \emph{\`a la} Dawson-Gärtner defined by
\begin{equ}
  S^0(\mu(\cdot)) := \frac 12 \int_0^T \|\dot \mu_t - \LL_\infty(\mu_t)^*\mu(t)\|_{\mu_t}^2 \d t\,,
  \end{equ}
  where $\LL_\infty$ is the generator of \eref{e:sde0} in the limit $N \to \infty$.
\end{theorem}

\subsection{Connection to \cite{rvde18}}

Finally, using that $\lambda = N$ and the ansatz
\begin{equ}\label{e:ansatz}
q_s = q_s'+ N^{-1} q_s''\,,
\end{equ}
for the marginal of $Q$ at time $t$ we obtain for the first and second order of \eref{e:lbar}
\begin{equ}\label{e:minimizer}
  \dtp{f,q_t'} - \dtp{f, q_0'} = \int_0^t N \dtp{f \dtp{\nabla_1 V,q_s'},q_s'} + \dtp{ f , \dtp{\nabla_1 V,q_s'},  q_s''} + \dtp{f ,  \dtp{\nabla_1 V,q_s''},  q_s'} +\dtp{f, \Delta q_s'} \d s\,,
\end{equ}
where $\dtp{\nabla_1 V,q}$ represents the integration of $\nabla_1 V$ in its second coordinate against the measure $q$.
\eref{e:minimizer} has a weak solution for all $f$ as above and all $N$ if for all $x \in \supp(q_s')$ we have to leading order in $N$ that
\begin{equ}
  \int_{\Rr^d}V(x,y) \d q_s'(y) = 0 \,.\label{e:constraint}
\end{equ}
If \eref{e:constraint} holds on the support of $q_s'$, however, the Laplacian operator on the \abbr{RHS} of \eref{e:minimizer} and the continuity of the potential $V$ imply that for any $t > 0$ the measure $q_s$ is absolutely continuous \abbr{wrt} the Lebesgue measure provided that \eref{e:constraint} is satisfied by the flow of $q_t'$ for all $t > 0$.
\footnote{By the continuity of the flow, the measure autside of the support of $q_0'$ should be going to $0$ as $t \to 0$. Then, by definition of $G(y) = \rho(y) \bar c(y)$, where $\bar c(y)$ is the average value of the weights for particles at $y$. However, this implies that if we start from a singular measure the average weights of the parameters in regions where $q_0'$ is singular diverge as $t \to \infty$. Note that even if this were possible (and it is only possible in the limit $N \to \infty$) the computational boundedness (and the required boundedness in \aref{a:1}?) of the phase space of the weights would prevent such a transition.}
\begin{figure}[t]
 \centering
  \def\svgwidth{.5\textwidth}
  \input{./figures/drawing-1.pdf_tex}
 \caption{Sketch of the dynamics of the minimizer $q_t$ as a function of time on the space of measures $M_+(\Rr^d)$. In the short timescale the minimizer should collapse onto $M_+^*(\Rr^d)$ (dashed line) and then, on the long timescales, drift to a local minimum $q_\infty$ of the potential landscape. }
 \label{f:f1}
\end{figure}
Assuming that this is the case we restrict our attention to the subspace $M_+^*(\Rr^d)$ where \eref{e:constraint} holds and the first two terms on the \abbr{rhs} of \eref{e:minimizer} cancel out to the leading order. Consistently with \cite{rvde18} we write $V(y,y') := -F(y) + K(y,y') - 1/N \log m(y)$ for $m~:~\Rr^d \to \Rr$ with compact sublevel sets. We note that this choice of $V$ satisfies \aref{a:1}: there exists a (compact) measure space $(\Omega, \mu)$ and functions $\vphi~:~\Omega \times \Rr^d \to \Rr$ and $f~:~\Omega \to \Rr$ such that
\begin{equ}\label{e:defphi}
  K(y,y') := \int_{\Omega} \vphi(x,y)\vphi(x,y') \d \mu(x) \quad \text{and} \quad F(y) = \int_\Omega f(x) \vphi(x,y) \d \mu(x)\,.
\end{equ}
We now write
\begin{equ}
\int  V(y,y') \d q_t'(y') \d q_t''(y) =  \int  K(y,y')-F(y) \d q_t''(y) \d q_t'(y') =  \int  K(y,y') \d q_t''(y) \d q_t'(y') + C_F(q_t'')    \,.
\end{equ}
and note that we can interpret the dynamics \eref{e:minimizer} as a gradient flow in the Wasserstein metric minimizing the energy
\begin{equ}\label{e:lagrangemult}
  \mathcal E[q_t']  =  \int \pc{  \log \frac{\d q_t'}{\d m}(y') + \int  (K(y,y')-F(y)) \d q_t''(y) }\d q_t'(y')\,.
  \end{equ}
  Realizing that the second term can be regarded as a Lagrange multiplier term enforcing \eref{e:constraint} we recover the result from  \cite[Proposition A.2]{rvde18}. Furthermore, denoting by $q_t^*$ the minimizer of \eref{e:lagrangemult}, the variation of the action $\mathscr S(q) := \int \log (\d q/\d m)\,\d q$ \abbr{wrt} the constraint $F(y)$
  \begin{equ}
    \delta^*(y) := \frac{\delta}{\delta\,F(y)} \mathscr S(q_t^*)
    \end{equ}
    gives the value of the costate $\delta = \beta \dtp{K,q_t''}$ at optimality. Using \eref{e:defphi} we define the error $\epsilon^*$ at optimality by
    \begin{equ}
      \delta^*(y) = \int \epsilon^*(x) \vphi(x,y) \d \mu(x)\,.
    \end{equ}
    Finally, denoting by $D_{f(x)}$ the gradient \abbr{wrt} $f$ in the $L^2(\Omega, \mu)$ norm (\abbr{cfr} \cite[(2.67)]{rvde18}) and using that $D_{f(x)} F(x) = \vphi(x,y)$ we obtain
    \begin{equ}
      \epsilon^*(x) = D_{f(x)} \mathscr S(q_t^*)\,,
    \end{equ}
    where $q_t^*$ is viewed as a functional of $f(x)$ and for any bounded test function $\xi~:~\Rr^d \to \Rr$  we write
    \begin{equ}
      \lim_{t \to \infty} \mathbb E \int \chi(x) \int \vphi(x,y) \d q_t''(y) \d \mu(x) = \beta^{-1} \int  \chi(x) \epsilon^*(x) \d \mu(x)\,,
    \end{equ}
    thereby recovering the result from \cite[Proposition A.3]{rvde18}.

\rmk{Some of the problems and open questions in the analysis carried out above are listed below.
\begin{myitem}
  \item The expansion \eref{e:ansatz} is motivated by an analysis from \cite{rvde18}, where it is shown that on the $\OO(N)$ timescales the previous order terms ($\OO(N^{a})$) for $a \in (1/2,1)$ disappear. This analysis is formal and this result should probably emerge as a product of the LDP, and not as an ansatz.
  \item For a compact measure space, is it really clear that one can continuously satisfy \eref{e:constraint} along a flow obeying a McKean-Vlasov \abbr{pde} with diffusion as $t \to 0$? In particular, provided that we start with a support that is singular \abbr{wrt} the lebesgue measure, it seems that we will be ``projected'' onto a measure that has full support, but which is at finite $W_2$ distance from the initial measure we started with ($c$s will be very large in regions of small density.)
  \item The notion of convergence of the measure $\mu^N$ to the mean field product measure can be made stronger by applyig the arguments of \cite{baz99}. To do so, one requires differentiability (as opposed to continuity) of $g$ in \aref{a:1}, along with nondegeneracy and uniqueness of the minimum of $\FF_T$.
\end{myitem}}

  \bibliographystyle{JPE.bst}
  \bibliography{bib.bib}

\end{document}
